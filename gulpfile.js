'use strict';

var gulp = require('gulp'),
  postcss = require('gulp-postcss'),
  sass = require('gulp-sass'),
  sassGlob = require('gulp-sass-glob'),
  sourcemaps = require('gulp-sourcemaps'),
  autoprefixer = require('autoprefixer'),
  plumber = require('gulp-plumber'),
  connect = require('gulp-connect'),
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant'),
  gulpif = require('gulp-if'),
  mqpacker = require("css-mqpacker"),
  uglify = require('gulp-uglify'),
  concat = require('gulp-concat'),
  babel = require('gulp-babel'),
  fileInclude = require('gulp-file-include'),
  fs = require('fs'),
  dirs = {
    'source': {
      'fonts': './source/fonts/*.*',
      'fontsFolder': './source/fonts/',
      'html': './source/pages/*.html',
      'html_watch': './source/pages/**/*.html',
      'sass': ['./source/sass/*.*', './source/sass/*.scss'],
      'sassRoot': 'source/sass/',
      'sass_watch': 'source/sass/**/*.scss',
      'img': ['./source/img/**/*.jpg', './source/img/**/*.png', './source/img/**/*.svg']
    },
    'build': {
      'css': './build/css/',
      'cssBuild':'../../../../openserver/domains/bubble-agency.develope/sites/all/themes/new/css',
      'cssVendor': './build/css/vendor/',
      'fonts': './build/fonts/',
      'html': './build',
      'img': './build/img/'
    }
  };

//fonts
gulp.task('fonts', function() {
  gulp.src(dirs.source.fonts)
    .pipe(gulp.dest(dirs.build.fonts));
});

//html
gulp.task('html', function() {
  gulp.src(dirs.source.html)
    .pipe(fileInclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest(dirs.build.html))
    .pipe(connect.reload());
});

//livereload server
gulp.task('connect', function() {
  connect.server({
    root: dirs.build.html,
    livereload: true,
    port: 9999
  });
});

//sass
gulp.task('compileSass', function() {

  var processors = [
    autoprefixer({ browsers: ['last 2 version', 'IE 10', 'IE 11'], cascade: false })
    // mqpacker({
    //   sort: function(a, b) {
    //     a = a.replace(/\D/g, '');
    //     b = b.replace(/\D/g, '');
    //     return a - b;
    //   }
    // })
  ];

  return gulp.src(dirs.source.sass)
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sassGlob())
    .pipe(sass({
      outputStyle: 'compact'
    }).on('error', sass.logError))
    .pipe(postcss(processors))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(dirs.build.css))
    .pipe(connect.reload());
});

//img
gulp.task('images', function() {
  return gulp.src(dirs.source.img)
    .pipe(plumber())
    .pipe(gulpif(/[.](png|jpeg|jpg|svg)$/, imagemin({
      progressive: true,
      svgoPlugins: [{
        removeViewBox: false
      }],
      use: [pngquant()]
    })))
    .pipe(gulp.dest(dirs.build.img))
    .pipe(connect.reload());
});


gulp.task('watch', function() {
  gulp.watch(dirs.source.html_watch, ['html']);
  gulp.watch(dirs.source.sass, ['compileSass']);
  gulp.watch(dirs.source.sass_watch, ['compileSass']);
  // gulp.watch(dirs.source.js, ['assembleJs']);
  gulp.watch(dirs.source.img, ['images']);
});

gulp.task('default', ['fonts', 'images', 'html', 'connect', 'compileSass', 'watch']);